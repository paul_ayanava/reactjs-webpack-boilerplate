import { render } from '@testing-library/react'
import '@testing-library/jest-dom'
import { LinkItem } from './LinkItem'
import { BrowserRouter } from 'react-router-dom'

describe('LinkItem', () => {
  test('should render link components', () => {
    const { getByText } = render(
      <BrowserRouter>
        <LinkItem link='/'>Home</LinkItem>
      </BrowserRouter>
    )
    expect(getByText('Home')).toBeInTheDocument()
  })
})
