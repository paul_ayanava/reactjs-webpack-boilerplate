import React from 'react'
import { Link } from 'react-router-dom'
import PropTypes from 'prop-types'
import './styles.scss'

export const LinkItem = ({ link, className, children, variant }) => (
  <Link to={link} className={`link ${className} link--${variant}`}>
    {children}
  </Link>
)

LinkItem.propTypes = {
  link: PropTypes.string.isRequired,
  children: PropTypes.any.isRequired,
  className: PropTypes.string,
  variant: PropTypes.string
}

LinkItem.defaultProps = {
  className: 'link'
}
