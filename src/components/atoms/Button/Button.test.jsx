import { render, cleanup, fireEvent } from '@testing-library/react'
import '@testing-library/jest-dom'
import { Button } from './Button'

describe('Button.jsx', () => {
  it('captures clicks', (done) => {
    function goToPreviousSlide() {
      done()
    }
    const { getByText } = render(
      <Button onClick={goToPreviousSlide}>prev</Button>
    )
    const node = getByText('prev')
    fireEvent.click(node)
  })
  afterEach(cleanup)
})
