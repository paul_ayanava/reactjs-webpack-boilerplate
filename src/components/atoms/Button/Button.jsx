import React from 'react'
import PropTypes from 'prop-types'

export const Button = ({ onClick, children, ...restProps }) => {
  return (
    <button onClick={onClick} {...restProps}>
      {children}
    </button>
  )
}

Button.propTypes = {
  onClick: PropTypes.any.isRequired,
  children: PropTypes.any.isRequired
}
